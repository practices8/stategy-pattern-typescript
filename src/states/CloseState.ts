import {IState} from "../interfaces/IState";
import {Internet} from "../app";
import {OpenState} from "./OpenState";

export class CloseState implements IState{
    private internet: Internet;
    constructor(internet:Internet) {
        this.internet = internet;
    }

    enter(): void {
        console.log("CloseS: ENTER");
        this.internet.changeState(new OpenState(this.internet));
    }

    error(): void {
        console.log("CloseS: ERROR");
    }

}