import {IState} from "../interfaces/IState";
import {Internet} from "../app";
import {CloseState} from "./CloseState";

export class OpenState implements IState{
    constructor(private internet: Internet) {}
    enter(): void {
        console.log("Opens: ENTER")
    }

    error(): void {
        console.log("Opens: ERROR")
        this.internet.changeState(new CloseState(this.internet));
    }
}