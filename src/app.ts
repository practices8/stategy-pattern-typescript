import {CloseState} from "./states/CloseState";
import {IState} from "./interfaces/IState";

export class Internet {
    private state: IState;
    constructor() {
        this.state = new CloseState(this);
    }

    changeState(state:IState){
        this.state = state;
    }

    error() {
        this.state.error()
    }

    enter() {
        this.state.enter();
    }
}
 const internet  = new Internet();

internet.error();
internet.enter();
internet.enter();
internet.enter();
internet.error();
internet.error();
internet.error();
internet.enter();
internet.enter();
