"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Internet = void 0;
const CloseState_1 = require("./states/CloseState");
class Internet {
    constructor() {
        this.state = new CloseState_1.CloseState(this);
    }
    changeState(state) {
        this.state = state;
    }
    error() {
        this.state.error();
    }
    enter() {
        this.state.enter();
    }
}
exports.Internet = Internet;
const internet = new Internet();
internet.error();
internet.enter();
internet.enter();
internet.enter();
internet.error();
internet.error();
internet.error();
internet.enter();
internet.enter();
//# sourceMappingURL=app.js.map