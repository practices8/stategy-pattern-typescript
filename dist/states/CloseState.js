"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CloseState = void 0;
const OpenState_1 = require("./OpenState");
class CloseState {
    constructor(internet) {
        this.internet = internet;
    }
    enter() {
        console.log("CloseS: ENTER");
        this.internet.changeState(new OpenState_1.OpenState(this.internet));
    }
    error() {
        console.log("CloseS: ERROR");
    }
}
exports.CloseState = CloseState;
//# sourceMappingURL=CloseState.js.map