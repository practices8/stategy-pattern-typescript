"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenState = void 0;
const CloseState_1 = require("./CloseState");
class OpenState {
    constructor(internet) {
        this.internet = internet;
    }
    enter() {
        console.log("Opens: ENTER");
    }
    error() {
        console.log("Opens: ERROR");
        this.internet.changeState(new CloseState_1.CloseState(this.internet));
    }
}
exports.OpenState = OpenState;
//# sourceMappingURL=OpenState.js.map